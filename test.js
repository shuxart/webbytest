/**
 * Created by shuxart on 1/19/15.
 */

var Animal, Dog, dog;

Animal = function constructor(name) {
    this.name = name;
};

Animal.prototype.getName =  function getName() {
    return this.name;
};

Dog = function constructor(name) {
    Animal.apply(this,arguments);
};
Dog.prototype = Object.create(Animal.prototype);

Dog.prototype.gav =  function gav() {
    return 'Dog ' + this.name + ' is saying gav';
};

dog  = new Dog('Aban');
console.log(dog.toString());
console.log('garfield' === dog.getName());
console.log('Dog Aban is saying gav' === dog.gav());
console.log(dog instanceof Dog);
console.log(dog instanceof Animal);
